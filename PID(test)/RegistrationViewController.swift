//
//  RegistrationViewController.swift
//  PID(test)
//
//  Created by tomoki on 2018/07/18.
//  Copyright © 2018年 tomoki. All rights reserved.
//

import UIKit

class RegistrationViewController: UIViewController {
    
    @IBOutlet weak var emailTextBox: UITextField!
    @IBOutlet weak var passwordTextBox: UITextField!
    @IBOutlet weak var searchIDTextBox: UITextField!
    @IBOutlet weak var userNameTextBox: UITextField!
    

    override func viewDidLoad() {
        super.viewDidLoad()

        emailTextBox.placeholder = "emailを入力"
        self.view.addSubview(emailTextBox)
        passwordTextBox.placeholder = "passwordを入力"
        self.view.addSubview(passwordTextBox)
        searchIDTextBox.placeholder = "検索用IDを入力"
        self.view.addSubview(searchIDTextBox)
        userNameTextBox.placeholder = "ユーザーネームを入力"
        self.view.addSubview(userNameTextBox)
        
        
        
        // Do any additional setup after loading the view.
    }
    
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
