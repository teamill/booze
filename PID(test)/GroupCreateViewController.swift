//
//  GroupCreateViewController.swift
//  PID(test)
//
//  Created by tomoki on 2018/07/22.
//  Copyright © 2018年 tomoki. All rights reserved.
//

import UIKit

class GroupCreateViewController: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate {

    @IBAction func choosePicture(_ sender: Any) {
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            let pickerView = UIImagePickerController()
            pickerView.sourceType = .photoLibrary
            pickerView.delegate = self
            self.present(pickerView, animated: true,completion: nil)
        }
        
    }
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let image = info[UIImagePickerControllerOriginalImage] as! UIImage
        self.GroupImage.image = image
        self.dismiss(animated: true)
    }
    
    @IBAction func CreateGroupButton(_ sender: Any) {
        
        
    }
    @IBOutlet weak var GroupImage: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // mask
        self.GroupImage.layer.masksToBounds = true
        
    }
    
    override func viewDidLayoutSubviews() {
        self.GroupImage.layer.cornerRadius = self.GroupImage.frame.height / 2
    }
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesEnded(touches, with: event)
        for touch: UITouch in touches {
            let tag = touch.view!.tag
            if tag == 1 {
                dismiss(animated: true, completion: nil)
            }
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
