//
//  fadeInViewController.swift
//  PID(test)
//
//  Created by tomoki on 2018/07/18.
//  Copyright © 2018年 tomoki. All rights reserved.
//

import UIKit

class fadeInViewController: UIViewController {

    enum FadeType: NSTimeInterval {
        case
        Normal = 0.2,
        Slow = 1.0
    }
    extension UIView {
        func fadeIn(type: FadeType = .Normal, completed: (() -> ())? = nil) {
            fadeIn(duration: type.rawValue, completed: completed)
        }
        /** For typical purpose, use "public func fadeIn(type: FadeType = .Normal, completed: (() -> ())? = nil)" instead of this */
        func fadeIn(duration: NSTimeInterval = FadeType.Slow.rawValue, completed: (() -> ())? = nil) {
            alpha = 0
            hidden = false
            UIView.animateWithDuration(duration,
                                       animations: {
                                        self.alpha = 1
            }) { finished in
                completed?()
            }
        }
        func fadeOut(type: FadeType = .Normal, completed: (() -> ())? = nil) {
            fadeOut(duration: type.rawValue, completed: completed)
        }
        /** For typical purpose, use "public func fadeOut(type: FadeType = .Normal, completed: (() -> ())? = nil)" instead of this */
        func fadeOut(duration: NSTimeInterval = FadeType.Slow.rawValue, completed: (() -> ())? = nil) {
            UIView.animateWithDuration(duration
                , animations: {
                    self.alpha = 0
            }) { [weak self] finished in
                self?.hidden = true
                self?.alpha = 1
                completed?()
            }
        }
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
