//
//  ViewController.swift
//  PID(test)
//
//  Created by tomoki on 2018/07/18.
//  Copyright © 2018年 tomoki. All rights reserved.
//

import UIKit


class ViewController: UIViewController {
 
    @IBOutlet weak var logoImage: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func viewDidAppear(_ animated: Bool) {
        //少し縮小するアニメーション
        UIView.animate(withDuration: 0.3,
                                   delay: 1.0,
                                   options: UIViewAnimationOptions.curveEaseOut,
                                   animations: { () in
                                    self.logoImage.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
        }, completion: { (Bool) in
            
        })
        
        //拡大させて、消えるアニメーション
        UIView.animate(withDuration: 0.2,
                                   delay: 1.3,
                                   options: UIViewAnimationOptions.curveEaseOut,
                                   animations: { () in
                                    self.logoImage.transform = CGAffineTransform(scaleX: 1.2, y: 1.2)
                                    self.logoImage.alpha = 0
        }, completion: { (Bool) in
            self.logoImage.removeFromSuperview()
            self.performSegue(withIdentifier: "toFirstViewController", sender: self)
        })
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

