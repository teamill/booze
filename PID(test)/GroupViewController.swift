//
//  GroupViewController.swift
//  PID(test)
//
//  Created by tomoki on 2018/07/25.
//  Copyright © 2018年 tomoki. All rights reserved.
//

import UIKit
import Charts

class GroupViewController: UIViewController {
    @IBOutlet weak var raderChartTest: RadarChartView!
    
    var name: [String]!
    var drink: [Int]!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        name = ["しょう","ともき","ひなこ","おんな"]
        drink = [60,40,100,80]
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
   
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
